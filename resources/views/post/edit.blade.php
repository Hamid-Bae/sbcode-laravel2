@extends('template.master')

@section('content')
<div class="card card-primary">
    <div class="card-header">
    <h3 class="card-title">Edit Post {{$post->id}}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
<form role="form" method="post" action="/post/{{$post->id}}">
        @csrf
        @method('put')
        <div class="card-body">
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" id="title" value="{{old('title', $post->title)}}" placeholder="Enter title">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Body</label>
                    <input type="text" class="form-control" name="body" id="body" value="{{old('body', $post->body)}}" placeholder="Enter body">
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
@endsection

{{-- <div>
    <h2>Edit Post {{$post->id}}</h2>
    <form action="/post/{{$post->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" name="title" value="{{$post->title}}" id="title" placeholder="Masukkan Title">
            @error('title')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">body</label>
            <input type="text" class="form-control" name="body"  value="{{$post->body}}"  id="body" placeholder="Masukkan Body">
            @error('body')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div> --}}