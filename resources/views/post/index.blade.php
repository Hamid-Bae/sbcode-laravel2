@extends('template.master')

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Post Table</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      @if (session('success'))
          <div class="alert alert-success">
            {{ session('success') }}
          </div>
      @endif
      <a href="/post/create" class="btn-primary btn mb-2">Create New Post</a>
      <table class="table table-bordered">
        <thead>                  
          <tr>
            <th style="width: 10px">#</th>
            <th>Title</th>
            <th>Body</th>
            <th style="width: 40px">Action</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($post as $key => $post)
                <tr>
                  <td>{{ $key + 1 }}</td>
                  <td>{{ $post->title }}</td>
                  <td>{{ $post->body }}</td>
                  <td class="d-flex">
                    <a href="/post/{{$post->id}}" class="btn btn-info btn-sm mr-1">show</a>
                    <a href="/post/{{$post->id}}/edit" class="btn btn-default btn-sm mr-1">edit</a>
                    {{-- <a href="/post/{{$post->id}}" class="btn btn-info btn-sm">delete</a> --}}
                    <form action="/post/{{$post->id}}" method="post">
                      @csrf
                      @method('delete')
                      <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                  </td>
                </tr>
            @empty
            <tr>
                <td colspan="4" align="center">No Post</td>
            </tr>
            @endforelse
          {{-- <tr>
            <td>1.</td>
            <td>Update software</td>
            <td>
              <div class="progress progress-xs">
                <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
              </div>
            </td>
            <td><span class="badge bg-danger">55%</span></td>
          </tr> --}}
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
    {{-- <div class="card-footer clearfix">
      <ul class="pagination pagination-sm m-0 float-right">
        <li class="page-item"><a class="page-link" href="#">«</a></li>
        <li class="page-item"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item"><a class="page-link" href="#">»</a></li>
      </ul>
    </div> --}}
  </div>
@endsection

{{-- <a href="/post/create" class="btn btn-primary">Tambah</a>
<table class="table">
    <thead class="thead-light">
        <tr>
        <th scope="col">#</th>
        <th scope="col">Title</th>
        <th scope="col">Body</th>
        <th scope="col" style="display: inline">Actions</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($post as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->title}}</td>
                <td>{{$value->body}}</td>
                <td>
                    <a href="/post/{{$value->id}}" class="btn btn-info">Show</a>
                    <a href="/post/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    <form action="/post/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table> --}}