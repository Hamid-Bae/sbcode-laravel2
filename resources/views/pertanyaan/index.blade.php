@extends('template.master')

@push('css')
<link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
@endpush

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Tabel Pertanyaan</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      @if (session('success'))
          <div class="alert alert-success">
            {{ session('success') }}
          </div>
      @endif
      <a href="/pertanyaan/create" class="btn-primary btn mb-2">Buat Pertanyaan Baru</a>
      <table id="example1" class="table table-bordered">
        <thead>                  
          <tr>
            <th style="width: 10px">#</th>
            <th>Judul</th>
            <th>Isi</th>
            <th style="width: 40px">Action</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($pertanyaan as $key => $pertanyaan)
                <tr>
                  <td>{{ $key + 1 }}</td>
                  <td>{{ $pertanyaan->judul }}</td>
                  <td>{{ $pertanyaan->isi }}</td>
                  <td class="d-flex">
                    <a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-info btn-sm mr-1">show</a>
                    <a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-default btn-sm mr-1">edit</a>
                    {{-- <a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-info btn-sm">delete</a> --}}
                    <form action="/pertanyaan/{{$pertanyaan->id}}" method="post">
                      @csrf
                      @method('delete')
                      <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                  </td>
                </tr>
            @empty
            <tr>
                <td colspan="4" align="center">Tidak ada pertanyaan</td>
            </tr>
            @endforelse
        </tbody>
      </table>
    </div>
  </div>
@endsection

@push('script')
<script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush