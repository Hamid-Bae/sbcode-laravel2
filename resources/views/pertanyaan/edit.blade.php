@extends('template.master')

@section('content')
<div class="card card-primary">
    <div class="card-header">
    <h3 class="card-title">Edit Pertanyaan {{$pertanyaan->id}}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
<form role="form" method="post" action="/pertanyaan/{{$pertanyaan->id}}">
        @csrf
        @method('put')
        <div class="card-body">
            <div class="form-group">
                <label for="title">Judul</label>
                <input type="text" class="form-control" name="judul" id="title" value="{{old('judul', $pertanyaan->judul)}}" placeholder="Masukkan judul pertanyaan">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Isi</label>
                    <input type="text" class="form-control" name="isi" id="body" value="{{old('isi', $pertanyaan->isi)}}" placeholder="Masukkan pertanyaan">
                @error('isi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
@endsection